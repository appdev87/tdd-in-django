import hashlib
import time

from django.test import TestCase
from selenium import webdriver
from webdriver_manager.firefox import GeckoDriverManager
from .forms import HashForm

from .models import Hash
from django.core.exceptions import ValidationError


class FunctionalTestCase(TestCase):
    def setUp(self):
        self.browser = webdriver.Firefox(executable_path=GeckoDriverManager().install())

    def test_there_is_homepage(self):
        self.browser.get('http://127.0.0.1:8000')
        self.assertIn('Enter hash here:', self.browser.page_source)

    def test_hash_of_hello(self):
        self.browser.get('http://127.0.0.1:8000')
        text = self.browser.find_element_by_id("id_text")
        text.send_keys("hello")
        self.browser.find_element_by_name("submit").click()
        self.assertIn('2cf24dba5fb0a30e26e83b2ac5b9e29e1b161e5c1fa7425e73043362938b9824', self.browser.page_source)

    def test_bad_data(self):
        def bad_hash():
            hash = Hash()
            hash.hash = '2cf24dba5fb0a30e26e83b2ac5b9e29e1b161e5c1fa7425e73043362938b9824ggggg'
            hash.full_clean()

        self.assertRaises(ValidationError, bad_hash)

    def text_hash_ajax(self):
        self.browser.get('http:localhost:8000')
        text = self.browser.find_element_by_id('id_text')
        text.send_keys('hello')
        time.sleep(5)
        self.assertIn('2cf24dba5fb0a30e26e83b2ac5b9e29e1b161e5c1fa7425e73043362938b9824', self.browser.page_source)

    def tearDown(self):
        self.browser.quit()


class UnitTestCase(TestCase):

    def test_home_homepage_template(self):
        """
        Test for webpage
        :return:
        """
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'hashing/home.html')

    def test_hash_form(self):
        form = HashForm(data={'text': 'hello'})
        self.assertTrue(form.is_valid())

    def test_hash_func_works(self):
        text_hash = hashlib.sha256('hello world'.encode('utf-8')).hexdigest()
        self.assertEqual('b94d27b9934d3e08a52e52d7da7dabfac484efe37a5380ee9088f7ace2efcde9', text_hash)

    @classmethod
    def __save_hash__(cls):
        hash = Hash()
        hash.text = 'hello world'
        hash.hash = 'b94d27b9934d3e08a52e52d7da7dabfac484efe37a5380ee9088f7ace2efcde9'
        hash.save()
        return hash

    def test_hash_object(self):
        hash = self.__save_hash__()
        pulled_hash = Hash.objects.get(hash='b94d27b9934d3e08a52e52d7da7dabfac484efe37a5380ee9088f7ace2efcde9')
        self.assertEqual(hash.text, pulled_hash.text)

    def test_viewing_hash(self):
        hash = self.__save_hash__()
        response = self.client.get('/hash/b94d27b9934d3e08a52e52d7da7dabfac484efe37a5380ee9088f7ace2efcde9')
        self.assertContains(response, 'hello world')

    def test_bad_data(self):
        def badHash():
            hash = Hash()
            hash.hash = 'b94d27b9934d3e08a52e52d7da7dabfac484efe37a5380ee9088f7ace2efcde9dhjwuhdwehudwehuidwehijdwe'
            hash.full_clean()

        self.assertRaises(ValidationError, badHash)
